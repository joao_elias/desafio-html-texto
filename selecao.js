const selectableTextArea = document.querySelectorAll(".selectable-text-area");
const twitterShareBtn = document.querySelector("#twitter-share-btn");

selectableTextArea.forEach(elem => {
    elem.addEventListener("mouseup", selectableTextAreaMouseUp);
});

function selectableTextAreaMouseUp(event) {
    setTimeout(() => {
        const selectedText = window.getSelection().toString().trim();
        //console.log(selectedText);
        if(selectedText.length){
            const x = event.pageX;
            const y = event.pageY
            const twitterShareBtnWidth = Number(getComputedStyle(twitterShareBtn).width.slice(0,-2));
            const twitterShareBtnHeight = Number(getComputedStyle(twitterShareBtn).height.slice(0,-2));
            twitterShareBtn.style.left = `${x - twitterShareBtnWidth*0.5}px`;
            twitterShareBtn.style.top = `${y - twitterShareBtnWidth*1.25}px`;
            twitterShareBtn.style.display = "block";
            twitterShareBtn.classList.add("btnEntrance");
    }
    }, 0);
}

document.addEventListener("mousedown", documentMouseDown);

function documentMouseDown(event){
    if(getComputedStyle(twitterShareBtn).display==="block" && event.target.id!=="twitter-share-btn"){
        twitterShareBtn.style.display = "none";
        twitterShareBtn.classList.remove("btnEntrance");
        window.getSelection().empty();
    }

}

twitterShareBtn.addEventListener("click", twitterShareBtnClick);

function twitterShareBtnClick(event){
  highlight("FFD700");
}

function highlight(colour){
    document.designMode = "on";
    document.execCommand("BackColor", false, colour);
    document.designMode = "off";
}

function Iniciar() {

    document.designMode = "On";
    
}
